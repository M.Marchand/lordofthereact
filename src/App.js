import React from 'react';
import logo from './logo.svg';
import './App.css';

import{
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from 'react-router-dom';
import * as APIConfig from './APIConfig';


import axios from 'axios';
// console.log(process.env.REACT_APP_lotr_api_API_KEY)
export default class App extends React.Component {
  constructor(props){
      super(props);
      this.state=
     {
    books: [],
    movies: []
  }}



    componentDidMount() {
        const $this=this;
        const axiosInstance = axios.create({
            baseURL: 'https://the-one-api.herokuapp.com/'
        })
        const requestHandler = (request) => {
            // Modify request here
            // request.headers['URL_REQUEST'] = 'https://the-one-api.herokuapp.com/v1/movie';
            request.headers['Authorization'] = 'Bearer 08U-F8WjDZMwiEpHWuzr';
            return request
        }
        const successHandler = (response) => {
            console.log(response);

        }
        axiosInstance.interceptors.request.use(
            request => requestHandler(request)
        )
        axiosInstance.interceptors.response.use(
            response => successHandler(response),

        )
        axiosInstance.get('v1/movie' )



        axios.get('https://the-one-api.herokuapp.com/v1/book')
            .then(function (response) {
                const books= response.data.docs;

                //
                //
                $this.setState({books});
                // handle success
                // console.log(response);
            })
            .catch(function (error) {
                // handle error

            })
            .finally(function () {
                // always executed
            });

        axiosInstance.get('/v1/movie' )
            .then(function (response) {
                const movies= response.data.docs;
                console.log(response);

                //
                //
                $this.setState({movies});
                console.log(movies)
                // handle success

            })
            .catch(function (error) {
                // handle error

            })
            .finally(function () {
                // always executed
            });
    }








    render() {

        const bookList = this.state.books.map((book , index) => {
            return(

                <li key={index}>Name:{book.name}</li>

            )
        });
        const movieList = this.state.movies.map((movie , index) => {
            return(

                <li key={index}>Name:{movie.name}</li>

            )
        })

return(
<div>
    <h1>BookList</h1>

    {bookList}

        <h1>MovieList</h1>

        {movieList}

</div>


)





  }

// componentDidMount(){
//     const  $this=this;
//     const axiosInstance = axios.create({
//         baseURL: 'https://the-one-api.herokuapp.com/'
//     })
//     const requestHandler = (request) => {
//         // Modify request here
//         // request.headers['URL_REQUEST'] = 'https://the-one-api.herokuapp.com/v1/movie';
//         request.headers['Authorization'] = 'Bearer 08U-F8WjDZMwiEpHWuzr';
//         return request
//     }
//     const successHandler = (response) => {
//         console.log(response);
//
//     }
//     axiosInstance.interceptors.request.use(
//         request => requestHandler(request)
//     )
//     axiosInstance.interceptors.response.use(
//         response => successHandler(response),
//
//     )
//     axiosInstance.get('v1/movie' )
//     axiosInstance.get('/v1/movie' )
//         .then(function (response) {
//             const movies= response.data.docs;
//             console.log(response);
//
//             //
//             //
//             $this.setState({movies});
//             console.log(movies)
//             // handle success
//
//         })
//         .catch(function (error) {
//             // handle error
//
//         })
//         .finally(function () {
//             // always executed
//         });
//
// }
//     render() {
//
//         const movieList = this.state.movies.map((movie, index) => {
//             return (
//
//                 <li key={index}>Name:{movie.name}</li>
//
//             )
//         })
//         return(
//             <div>
//
//
//                 <h1>MovieList</h1>
//
//                 {movieList}
//
//             </div>
//
//
//         )
//     }




}
